/// <reference types="cypress" />
import Chance from 'chance';
const chance = new Chance();

describe ('Testing the search feature', function(){

  beforeEach(() => {
    let credentials = require('../fixtures/example.json')
    cy.login(credentials.env_email, credentials.env_password)
  })

      it('Tests sucessfull search', function() {

        cy.get('tbody > :nth-child(1) > :nth-child(1)').invoke('text')
          .then( text => { cy.get('#buscaNome').type(text) }) 
        cy.get('#botaoBuscar').click()
        cy.get('#buscaNome').invoke('val').then( text => {
          cy.log(text)
          cy.get('tr > :nth-child(1)').should('contain', text) 
        })
        
        

        //cy.get('tr > :nth-child(1)').should('contain', text)

        
      })
    

});