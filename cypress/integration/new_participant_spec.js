/// <reference types="cypress" />
import Chance from 'chance';
const chance = new Chance();

describe ('Testing the new participant feature', function(){

  beforeEach(() => {
    var credentials = require('../fixtures/example.json')
    cy.login(credentials.env_email, credentials.env_password)
  })

      it('Creates new participant', function() {

        var email = chance.email();
        var birthday = "14021995"
        var fieldTest = chance.string({ length: 7, pool: 'abcde' });
        var text = chance.string({ length: 20, pool: 'abcde' });
        var city = chance.city()
        var UFvalue = "SC"
        var parentName = chance.name()
        var name = chance.name()
        var validRG = "173257999"
        var orgao = "SSP"
        var cpf = chance.cpf()
        var civilState = "Solteiro"
        var phone = chance.phone({ formatted: false });
        var cep = chance.zip({plusfour: true});
        var street = chance.street();
        var number = chance.integer({ min: 0, max: 15 })
        var selectYes = "Sim"
        var selectNo = "Não"

        cy.get('#novoParticipante').click();

      //fills all fields 
        cy.get('#full-name')
         .type(name).should('have.value', name)

         cy.get('#date-birth')
          .type(birthday)

         cy.get('#nacionality')
          .type(fieldTest).should('have.value', fieldTest)

         cy.get('#city')
          .type(city).should('have.value', city)

         cy.get('#uf')
         .select(UFvalue).should('have.value', UFvalue)

         cy.get('#father-name')
          .type(parentName).should('have.value', parentName )

         cy.get('#mother-name')
         .type(parentName).should('have.value', parentName )

         cy.get('#rg')
         .type(validRG).should('have.value', validRG)

         cy.get('#orgao-emissor')
         .type(orgao).should('have.value', orgao)

         cy.get('#cpf')
         .type(cpf).should('have.value', cpf)

         cy.get('#inputEstadoCivil')
         .select(civilState).should('have.value', civilState)

         cy.get('#telefoneRes')
         .type(phone)

         cy.get('#telefoneCel')
         .type(phone)

         cy.get('#email')
         .type(email).should('have.value', email)

         cy.get('#cep')
         .type(cep)

         cy.get('#rua')
         .type(street).should('have.value', street)

         cy.get('#numero')
         .type(number).should('have.value', number)

         cy.get('#bairro')
         .type(fieldTest).should('have.value', fieldTest)

         cy.get('#complemento')
         .type(number).should('have.value', number)

         cy.get('#cidade')
         .type(city).should('have.value', city)

         cy.get('#uf-endereco')
         .select(UFvalue).should('have.value', UFvalue)

         cy.get('#inputProfissao')
         .type(fieldTest).should('have.value', fieldTest)

         cy.get('#aposentadoria')
         .select(selectNo)

         cy.get('#inputLocalTrabalho')
         .type(fieldTest).should('have.value', fieldTest)

         cy.get('#telComercial')
         .type(phone)

         cy.get('#cepComercial')
         .type(cep)

         cy.get('#logradouroComercial')
         .type(street).should('have.value', street)

         cy.get('#numeroComercial')
         .type(number).should('have.value', number)

         cy.get('#bairroComercial')
         .type(fieldTest).should('have.value', fieldTest)

         cy.get('#complementoComercial')
         .type(fieldTest).should('have.value', fieldTest)

         cy.get('#cidadeComercial')
         .type(city).should('have.value', city)

         cy.get('#ufComercial')
         .select(UFvalue).should('have.value', UFvalue)

         cy.get('#escolaridade')
         .select("Outros")

         cy.get('#cursoEscolaridade')
         .type(fieldTest).should('have.value', fieldTest)

         cy.get('#instituicaoEscolaridade')
         .type(fieldTest).should('have.value', fieldTest)

         cy.get('#descricaoAtividades')
         .type(text).should('have.value', text)

         cy.get('#transferencia')
         .select(selectYes)

         cy.get('#nomeCentroEspirita')
         .type(fieldTest).should('have.value', fieldTest)

         cy.get('#cidadeCentroEspirita')
         .type(city).should('have.value', city)

         cy.get('#ufCentroEspirita')
         .select(UFvalue).should('have.value', UFvalue)

         cy.get('#tempoCentroEspirita')
         .type(selectYes)

         cy.get('#tomosESede')
         .type(fieldTest).should('have.value', fieldTest)

         cy.get('#tomosEade')
         .type(fieldTest).should('have.value', fieldTest)

         cy.get('#obrasBasicas')
         .type(fieldTest).should('have.value', fieldTest)

         cy.get('#outrasObras')
         .type(fieldTest).should('have.value', fieldTest)

         cy.get('#atividadeVoluntaria')
         .type(fieldTest).should('have.value', fieldTest)

         cy.get('#tempoAtividadeVoluntaria')
         .type(fieldTest).should('have.value', fieldTest)

         cy.get('#adicionaAtividade')
         .click

         cy.get('#removeAtividadeVoluntaria')
         .click

         cy.get('#mediumSensitivo')
         .check()

         cy.get('#mediumPsicofonico')
         .check()

         cy.get('#mediumPsicografo')
         .check()

         cy.get('#mediumPictografo')
         .check()

         cy.get('#mediumVidente')
           .check()

         cy.get('#mediumAudiente')
           .check()

         cy.get('#mediumDesdobramento')
           .check()

         cy.get('#dirigenteGrupoMediunico')
           .check()

         cy.get('#dialogador')
           .check()

         cy.get('#susutentacao')
           .check()

         cy.get('#outros')
           .check()
          fieldTest
         cy.get('#mediumSensitivoTempo')
           .type(fieldTest).should('have.value', fieldTest)

         cy.get('#mediumPsicofonicoTempo')
           .type(fieldTest).should('have.value', fieldTest)

         cy.get('#mediumPsicografoTempo')
           .type(fieldTest).should('have.value', fieldTest)

         cy.get('#mediumPictografoTempo')
           .type(fieldTest).should('have.value', fieldTest)

         cy.get('#mediumVidenteTempo')
           .type(fieldTest).should('have.value', fieldTest)

         cy.get('#mediumAudienteTempo')
           .type(fieldTest).should('have.value', fieldTest)

         cy.get('#mediumDesdobramentoTempo')
           .type(fieldTest).should('have.value', fieldTest)

         cy.get('#dirigenteGrupoMediunicoTempo')
           .type(fieldTest).should('have.value', fieldTest)

         cy.get('#dialogadorTempo')
           .type(fieldTest).should('have.value', fieldTest)

         cy.get('#susutentacaoTempo')
           .type(fieldTest).should('have.value', fieldTest)

         cy.get('#outrosTempo')
           .type(fieldTest).should('have.value', fieldTest)

         cy.get('#cadastrar')
          .click()

          //validates if user was created in the user table
         cy.visit('http://agriness-challenge.herokuapp.com/home')
         cy.contains(name)
        })

});