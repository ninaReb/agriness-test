/// <reference types="cypress" />
import Chance from 'chance';
const chance = new Chance();


describe ('Testing the registration feature', function(){

    beforeEach(() => {
        cy.visit('http://agriness-challenge.herokuapp.com/login')       
      });
      
      it('Creates a new user', function() {
        var email = chance.email();
        var name = chance.name()
        var pass = chance.string({ length: 8});

        cy.get('#cadastroNome').type(name).should('have.value', name)

        cy.get('#cadastroEmail').type(email).should('have.value', email)

        cy.get('#cadastroSenha').type(pass).should('have.value', pass)

        cy.get('#botaoCadastrar').click()
        cy.get('.navbar-text').should('contain', name)
        
      });

         



         

        

      
    

});