/// <reference types="cypress" />
import Chance from 'chance';
const chance = new Chance();

describe ('Testing the delete participant feature', function(){

    beforeEach(() => {
        var credentials = require('../fixtures/example.json')
        cy.login(credentials.env_email, credentials.env_password)
      })

      it('Deletes random user from the list', function() {
        var child = chance.integer({ min: 1, max: 5 })
        cy.log(child)
        cy.get(':nth-child('+ child + ') > :nth-child(6) > #deletarContato').click()
        cy.get('.s-alert-box-inner > p').should('exist')
      })


    

});