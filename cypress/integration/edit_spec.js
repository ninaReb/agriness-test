/// <reference types="cypress" />
import Chance from 'chance';
const chance = new Chance();

describe ('Testing the edit participant feature', function(){

  beforeEach(() => {
    var credentials = require('../fixtures/example.json')
    cy.login(credentials.env_email, credentials.env_password)
  })

      it('Editd random user from the list', function() {
        var child = chance.integer({ min: 1, max: 5 })
        var email = chance.email();
        var birthday = "14021995"
        var fieldTest = chance.string({ length: 7, pool: 'abcde' });
        var city = chance.city()
        var UFvalue = "PR"
        var parentName = chance.name()
        var name = chance.name()
        var validRG = "173257999"
        var orgao = chance.string({ length: 4, pool: 'abcde' });
        var cpf = chance.cpf()
        var civilState = "Solteiro"
        var phone = chance.phone({ formatted: false });
       
        cy.log(child)
        cy.get(':nth-child('+ child + ') > :nth-child(4) > #editarContato').click()

        cy.get('#full-name').clear()
         .type(name)

         cy.get('#date-birth').clear()
          .type(birthday)

         cy.get('#nacionality').clear()
          .type(fieldTest)

         cy.get('#city').clear()
          .type(city)

         cy.get('#father-name').clear()
          .type(parentName)

         cy.get('#mother-name').clear()
         .type(parentName)

         cy.get('#rg').clear()
         .type(validRG)

         cy.get('#orgao-emissor').clear()
         .type(orgao)

         cy.get('#cpf').clear()
         .type(cpf)

         cy.get('#telefoneRes').clear()
         .type(phone)

         cy.get('#telefoneCel').clear()
         .type(phone)

         cy.get('#email').clear()
         .type(email)

         cy.get('#editar').click()

         cy.get('.s-alert-box-inner > p').should('exist')
      })
    

});